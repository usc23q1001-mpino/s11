from django.urls import path
from . import views

# Syntax
# path(route, view, name)
app_name = 'todolist'
urlpatterns = [
    # /todolist route
    path('', views.index, name='index'),
    path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
    path('register', views.register, name='register'),
    path('update_profile/<int:user_id>', views.update_profile, name='update_profile'),
   
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
   
    path('add_task', views.add_task, name='add_task'),
    path('edit/<int:todoitem_id>', views.update_task, name='update_task'),
    path('delete/<int:todoitem_id>', views.delete_task, name='delete_task'),
    
    path('add_event', views.add_event, name='add_event'),
    path('events/<int:event_id>', views.todoevent, name='viewevent'),
]